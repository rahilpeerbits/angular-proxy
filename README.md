# How to Run

- Clone repo with git clone https://gitlab.com/rahilpeerbits/angular-proxy.git
- cd angular-proxy
- npm install
- npm start


# Customizable files

- Change your API url in proxy.conf.json file
- For production add API url in environment.prod.ts

