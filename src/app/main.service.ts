import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { version } from 'process';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MainService {
  [x: string]: any;

  apiUrl = environment.apiUrl;
  constructor(private http: HttpClient) { }

  public getToken(
    data: any
  ): Observable<any> {
    const options = { headers: this.setHeaders() };
    return this.http.post<any>(`${this.apiUrl}/user/gettoken`, data, options);
  }

  private setHeaders(): HttpHeaders {
    const headersConfig = {
      Accept: 'application/json',
      'cache-control': 'no-cache',
      'Content-Type': 'application/x-www-form-urlencoded',
    };
    return new HttpHeaders(headersConfig);
  }
}
