import { Component, OnInit } from '@angular/core';
import { MainService } from './main.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'proxyTesting';

  constructor(private mainService: MainService) {}

  ngOnInit() {
    this.mainService.getToken({
      device_id: '124',
      device_type: 'a'
    }).subscribe(res => {
      console.log(res);
    });
  }
}
